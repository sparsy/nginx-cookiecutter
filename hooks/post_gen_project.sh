#!/usr/bin/env sh

cd ..

# moving dir name to different name from inner file
mv {{cookiecutter.name}} {{cookiecutter.name}}-tmp

mv {{cookiecutter.name}}-tmp/{{cookiecutter.name}} .

rmdir {{cookiecutter.name}}-tmp

echo "Remember to do a symbolic link in 'site-enabled/'!"
